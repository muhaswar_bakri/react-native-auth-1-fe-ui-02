import React from 'react';
import {View, Text, Button } from 'react-native';

const Home = ({logout}) => {

  return (
    <View style={{
      flex:1,
      justifyContent: 'center',
      alignItems:'center'
    }}>
      <Text> You already in </Text>
      <Button title="logout" onPress={logout}/>
    </View>
  );
}

export default Home;